var express = require("express");
var router = express.Router();
var db = require("../database/dbconnection");

router.get("/", function (req, res, next) {
  console.log("Base URL");
  console.log(req.params);
  res.locals.title = "MarkTech";
  res.status(200);
  res.render("index");
});

router.get("/api", function (req, res, next) {
  console.log("Base URL");
  console.log(req.params);
  res.locals.title = "MarkTech";
  res.status(200);
  res.render("index");
});

router.get("/:path", function (req, res, next) {
  if (req.params.path.indexOf('.') !== -1) {
    return;
  }
  console.log("Base URL with path");
  console.log(req.params.path);
  let current_timestamp = Math.round(new Date().getTime() / 1000);
  values = {
    click_timestamp: current_timestamp
  };
  db.query(
    `select cs.redirect_url from campaign_schedule cs \
    join campaign_report cr on cr.campaign_id = cs.id \
    where cr.unique_path = '${req.params.path}'`,
    function (err, row) {
      if (err) {
        console.log(err);
      } else {
        if (row.length == 0) {
          handlePageNotFound(res);
        } else {
          res.redirect(row[0].redirect_url);
          db.query(
            "Update campaign_report \
                 SET ? where unique_path = ?",
            [values, req.params.path],
            function (err, row) {
              if (err) {
                console.log(err);
              } else {
                console.log(row);
              }
            }
          );
        }
      }
    }
  );
});

function handlePageNotFound(res) {
  res.locals.message = "Page Not Found";
  res.locals.error = {};
  res.status(404);
  res.render('error');
}

module.exports = router;
