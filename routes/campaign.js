var express = require('express');
var router = express.Router();
var model = require('../models/campaignModel');
var baseUrlModel = require('../models/baseUrlModel');

var verifyToken = require('../verifyToken');
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get('/getBaseUrls', verifyToken, function (req, res, next) {
    baseUrlModel.getBaseUrls(req, function (err, data, message) {
        if (err) {
            res.statusCode = 500;
            res.json({ 'status': 'error', 'data': null, 'message': err });
        } else {
            res.statusCode = 200;
            res.json({ 'status': 'success', 'data': data, 'message': message });
        }
    });
});

router.post('/create', verifyToken, function (req, res, next) {
    console.log(req.body);
    var segment_id = req.body.segment_id;
    var message = req.body.message;
    var sender = req.body.sender_id;
    var numbers = req.body.numbers;

    if (numbers) {
        if (message && sender) {
            console.log('sendByNumbers');
            model.sendByNumbers(req.body, function (err, response) {
                if (err) {
                    res.statusCode = 500;
                    res.json({ 'status': 'error', 'message': err });
                } else {
                    res.statusCode = 200;
                    res.json({ 'status': 'success', 'message': 'Scheduled Successfully.' });
                }
            });
        } else {
            res.statusCode = 401;
            res.json({ 'status': 'error', 'message': 'sender or message is missing!' });
        }
    } else {
        if (segment_id && message && sender) {
            console.log('sendBySegment');
            model.sendBySegment(req.body, function (err, response) {
                if (err) {
                    res.statusCode = 500;
                    res.json({ 'status': 'error', 'message': err });
                } else {
                    res.statusCode = 200;
                    res.json({ 'status': 'success', 'message': 'Scheduled Successfully.' });
                }
            });
        } else {
            res.statusCode = 401;
            res.json({ 'status': 'error', 'message': 'segmentId, senderId or message is missing!' });
        }
    }
});

module.exports = router;