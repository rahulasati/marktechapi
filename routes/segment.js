var express = require('express');
var router = express.Router();
var segmentModel = require('../models/segmentModel');

var verifyToken = require('../verifyToken');
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.post('/createSegment', verifyToken, function (req, res, next) {
    console.log(req.body);
    var name = req.body.name;
    var desc = req.body.desc;
    var query = req.body.query;
    var props = req.body.props;

    if (name && (props || query)) {
        segmentModel.createSegment(req.body, function (err, data, message) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.statusCode = 200;
                res.json({ 'status': 'success', 'data': data, 'message': message });
            }
        });
    } else {
        res.statusCode = 400;
        res.json({ 'status': 'error', 'data': null, 'message': 'name, query or properties is missing!' });
    }
});

router.get('/getSegments', verifyToken, function (req, res, next) {
    segmentModel.getSegments(req, function (err, data, message) {
        if (err) {
            res.statusCode = 500;
            res.json({ 'status': 'error', 'data': null, 'message': err });
        } else {
            res.statusCode = 200;
            res.json({ 'status': 'success', 'data': data, 'message': message });
        }
    });
});

router.post('/updateSegment', verifyToken, function (req, res, next) {
    console.log(req.body);
    var segmentId = req.body.segment_id;
    var name = req.body.name;
    var desc = req.body.desc;
    var query = req.body.query;
    var props = req.body.props;

    if (segmentId && (props || query)) {
        segmentModel.updateSegment(req.body, function (err, data, message) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.statusCode = 200;
                res.json({ 'status': 'success', 'data': data, 'message': message });
            }
        });
    } else {
        res.statusCode = 400;
        res.json({ 'status': 'error', 'data': null, 'message': 'segmentId, query or props is missing!' });
    }
});

router.delete('/delete/:segment_id', verifyToken, function (req, res) {
    console.log(req.params);
    if (req.params.segment_id) {
        segmentModel.deleteSegment(req.params.segment_id, function (err, data, message) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.statusCode = 200;
                res.json({ 'status': 'success', 'data': data, 'message': message });
            }
        });
    } else {
        res.statusCode = 400;
        res.json({ 'status': 'error', 'data': null, 'message': 'segment_id is missing!' });
    }
});

module.exports = router;