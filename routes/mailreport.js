var express = require("express");

var mail = require('../models/mailReport');

var router = express.Router();

router.get("/mail", function(req, res) {
  mail.mailReport(req.query);
});

module.exports = router;
