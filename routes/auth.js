var express = require('express');
var router = express.Router();
var authModel = require('../models/authModel');
var validator = require('validator');

var verifyToken = require('../verifyToken');
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json()); 

router.post('/register', function (req, res, next) {
    var email = req.body.email;
    console.log(req.body);

    if (email == null || !validator.isEmail(email)) {
        res.statusCode = 401;
        res.json({ 'status': 'error', 'data': null, 'message': 'Please enter a valid email!' });
    } else if (req.body.password && req.body.first_name && req.body.last_name) {
        authModel.registerUser(req.body, function (err, response) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.json({ 'status': 'success', 'data': response, 'message': '' });
            }
        });
    } else {
        res.statusCode = 401;
        res.json({ 'status': 'error', 'data': null, 'message': 'first_name,last_name or password is missing!' });
    }
});

router.post('/login', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    console.log("email:" + email + " pwd:" + password);

    if (email && password) {
        authModel.loginUser(req.body, function (err, response) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.json({ 'status': 'success', 'data': response, 'message': '' });
            }
        });
    } else {
        res.statusCode = 401;
        res.json({ 'status': 'error', 'data': null, 'message': 'email or password is missing' });
    }
});

router.post('/change_password', verifyToken, function (req, res, next) {
    console.log(req.body);

    if (req.body.old_password == undefined || req.body.new_password == undefined) {
        res.statusCode = 401;
        res.json({ 'status': 'error', 'data': null, 'message': 'old_password/new_password  is missing!' });
    } else {
        authModel.changePassword(req.body, function (err, response) {
            if (err) {
                res.statusCode = 500;
                res.json({ 'status': 'error', 'data': null, 'message': err });
            } else {
                res.json({ 'status': 'success', 'data': response, 'message': '' });
            }
        });
    }
});

module.exports = router;