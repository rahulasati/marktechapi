var express = require('express');
var Bitly = require('bitlyapi');
var router = express.Router();

var verifyToken = require('../verifyToken');
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get('/create_url', verifyToken, function (req, res, next) {
    console.log(req.query);
    var redirect_url = req.query.redirect_url;
    var source = req.query.source;
    var medium = req.query.medium;
    var campaign = req.query.campaign;

    if (redirect_url) {
        var bitly = new Bitly('09824cebc8de57b2dd87db2eeb09b071133d20a7');
        //54.173.150.242
        var longUrl = "http://54.173.150.242:3000/track/callback?";

        for (var propName in req.query) {
            if (req.query.hasOwnProperty(propName)) {
                console.log(propName, req.query[propName]);
                longUrl += propName + "=" + req.query[propName] + "&";
            }
        }

        longUrl = longUrl.slice(0, -1);

        console.log(longUrl);

        bitly.shorten(longUrl)
            .then(function (response) {
                console.log(response);
                res.statusCode = 200;
                res.json({ 'status': 'success', 'data': response.data, 'message': '' });
            }, function (error) {
                res.statusCode = 500;
                console.log(error);
            });
    } else {
        res.statusCode = 401;
        res.json({ 'status': 'error', 'data': null, 'message': 'url to short is missing!' });
    }
});

router.get('/callback', function (req, res, next) {
    console.log('-----------track data to copy-------------');
    console.log(req.query);
    console.log('-----------track data to copy-------------');
    var redirectUrl = req.query.redirect_url || '';
    var source = req.query.source || '';
    var medium = req.query.medium || '';
    var campaign = req.query.campaign || '';
    

    res.redirect(redirectUrl);
});

module.exports = router;