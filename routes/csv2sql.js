var express = require("express");
var router = express.Router();
var mysql = require('mysql');
var async = require("async");

var connectionPool = mysql.createPool({
  connectionLimit: 100,
  connectTimeout: 60 * 60 * 1000,
  aquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  host: "45.77.40.241",
  user: "master",
  password: "Rezolut@123",
  database: "master_dmp"
});

// var connectionPool = mysql.createPool({
//   connectionLimit: 100,
//   connectTimeout: 60 * 60 * 1000,
//   aquireTimeout: 60 * 60 * 1000,
//   timeout: 60 * 60 * 1000,
//   "host": "localhost",
//   "user": "root",
//   "password": "kuchbhi",
//   "database": "user_db"
// });

router.get("/", function (req, res, next) {
  const { exec } = require("child_process");
  exec('find /Volumes/amitk1/Downloads/good -name "*SOCIAL*.csv"', (err, stdout, stderr) => {
    if (err) {
      sendResponse(res, err, 500);
    } else {
      var rowsInserted = 0;
      let pathArray = stdout.trim().split("\n");
      var pathArray2 = pathArray.slice(164, pathArray.length);
      console.log(pathArray.length);
      console.log(pathArray2.length);
      let queryFuncArray = getQueryArray(pathArray2, res, rowsInserted);
      console.log(queryFuncArray.length);
      async.series(queryFuncArray, function (err, results) {
        console.log('*******************Final Result Total Inserted Rows**************************')
        console.log(rowsInserted);
        if (err) {
          console.log(err);
        } else {
          console.log(results);
        }
      });
      let successMessage = queryFuncArray.length + " files will be processed and hopefully all will be good :)"
      sendResponse(res, successMessage, 200);
    }
  });
});

function getQueryArray(paths, res, rowsInserted) {
  let arr = [];
  paths.map(function (path, index) {
    let column_name = path.split("/")[5];
    // console.log(index + " : " + path + " : " + column_name);
    arr.push(function (callback) {
      connectionPool.getConnection(function (err, connection) {
        if (err) {
          connection.release();
          sendResponse(res, "Error in connection database", 500);
        }
        connection.query(
          `LOAD DATA LOCAL INFILE '${path}' INTO TABLE master_dmp
        FIELDS TERMINATED BY ',' 
        ENCLOSED by '"'
        LINES TERMINATED BY '\n'
        IGNORE 1 rows
        SET affinity = '${column_name}';`,
          function (err, row) {
            console.log('*********************************************')
            console.log(index + " : " + path + " : " + column_name);
            if (err) {
              console.log(err);
            } else {
              console.log(row);
              rowsInserted += row.affectedRows;
            }
            console.log('*********************************************')
            callback(null, index);
          }
        );
      });
    });
  });
  return arr;
}

function sendResponse(res, message, statusCode) {
  res.locals.message = message;
  res.locals.error = {};
  res.status(statusCode);
  res.render('error');
}

module.exports = router;