var db = require('../database/dbconnection');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var config = require('../config');

module.exports = {

    registerUser: function (userData, callback) {
        registerUser(userData, callback);
    },

    loginUser: function (loginData, callback) {
        var query = "SELECT * FROM agent_data where email = ?";
        console.log(query);
        db.query(query, [loginData.email], function (err, rows) {
            if (err) {
                callback(err, rows);
            } else {
                if (rows.length > 0) {
                    console.log(rows[0]);
                    //user exist with this email so lets compare his password
                    bcrypt.compare(loginData.password, rows[0].password, function (err, res) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (res == true) {
                                var userId = rows[0].id;
                                //password match login succesful
                                var token = jwt.sign({ id: userId }, config.secret);
                                delete rows[0].password;
                                delete rows[0].id;
                                rows[0].user_id = userId;
                                rows[0].token = token;
                                callback(null, rows[0]);
                            } else {
                                callback('password is incorrect.', null);
                            }
                        }
                    });
                } else {
                    //user does not exist with this phone
                    callback('User Does not exist, please check again or register.', null);
                }
            }
        });
    },

    // updateUser: function (userData, callback) {

    //     var values = {
    //         name: userData.name || undefined,
    //         email: userData.email || undefined
    //     };

    //     values = JSON.stringify(values);
    //     values = JSON.parse(values);
    //     console.log(values);

    //     db.query("UPDATE agent SET ? WHERE id = ?", [values, userData.user_id], function (err, rows) {
    //         if (err) {
    //             callback(err, rows, null);
    //         } else {
    //             console.log(rows.affectedRows);
    //             if (rows.affectedRows > 0) {
    //                 var query = "SELECT * FROM agent where id = " + userData.user_id;
    //                 db.query(query, function (err, rows) {
    //                     if (rows.length > 0) {
    //                         delete rows[0].id;
    //                         rows[0].user_id = userData.user_id;
    //                         callback(null, rows[0], 'Agent details updated successfully');
    //                     }
    //                 });
    //                 //callback(null, 'Agent details updated successfully');
    //                 if (userData.phone) {
    //                     updateIntoAuthUser(userData);
    //                 }
    //             } else {
    //                 callback('User Id is incorrect.', null, null);
    //             }
    //         }
    //     });
    // }, 

    changePassword: function (reqData, callback) {
        db.query("select password from agent_data where id = ? ", reqData.user_id, function (err, rows) {
            if (err) {
                callback(err, rows);
            } else {
                if (rows.length > 0) {
                    //user exist with this id so lets compare his password
                    bcrypt.compare(reqData.old_password, rows[0].password, function (err, res) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (res == true) {
                                //provided password matches so lets update it with new one.
                                bcrypt.genSalt(10, function (err, salt) {
                                    bcrypt.hash(reqData.new_password, salt, function (err, hashkey) {
                                        db.query("UPDATE agent_data SET ? WHERE id = ?", [{ password: hashkey }, reqData.user_id], function (err, rows) {
                                            if (err) {
                                                callback(err, rows);
                                            } else {
                                                console.log(rows);
                                                if (rows.affectedRows > 0) {
                                                    callback(null, 'password changed successfully.');
                                                } else {
                                                    callback(rows, null);
                                                }
                                            }
                                        });
                                    });
                                });
                            } else {
                                callback('password is incorrect.', null);
                            }
                        }
                    });
                } else {
                    //user does not exist with this user-id
                    callback('User Id is incorrect.', null);
                }
            }
        });
    },
};

function registerUser(userData, callback) {
    db.query("select first_name from agent_data where email = ? ", userData.email, function (err, rows) {
        if (err) {
            console.log(err);
            callback(err, rows);
        } else {
            if (rows.length > 0) {
                console.log('Agent already with this email');
                callback('Agent with this email number already exist', 'duplicate_email');
            } else {
                console.log('insert new row');
                insertIntoCustomer(userData, callback);
            }
        }
    });
}

function insertIntoCustomer(userData, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(userData.password, salt, function (err, passwordHash) {
            userData.password = passwordHash;
            var values = {
                first_name: userData.first_name || "",
                last_name: userData.last_name || "",
                email: userData.email || "",
                role: userData.role || "agent",
                created_by: userData.user_id || "0",
                password: userData.password
            };
            console.log('------------------');
            console.log(values);
            return db.query("INSERT INTO agent_data set ?", values, function (err, row) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    var userId = row.insertId;
                    //user is created without any error
                    var token = jwt.sign({ id: userId }, config.secret);
                    delete values.password;
                    values.user_id = userId;
                    values.token = token;
                    console.log(values);
                    callback(err, values);
                }
            });
        });
    });
}