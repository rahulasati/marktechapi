var axios = require("axios");
var moment = require("moment");
var jsonToCSV = require("json-to-csv");

const sendmail = require("sendmail")();


  module.exports = {
    scheduleReport : function(advertiser,days) {
        var query = {
            advertiser : advertiser || "Play+Games24x7+Pvt.+Ltd",
            days: days || 1
        }
        mailReportFn(query)
        // console.log("scheduled.......");
      },
       mailReport: function(query){
           mailReportFn(query)
       }
  } 


function mailReportFn(query) {
        console.log(query);
        var page_count = 1;
        var response_data = {
          data: [],
          offerIds: []
        };
        var current_page = 1;
        var url = getBaseURL(query);
        let res_url = url + "&page=" + current_page;
        console.log(res_url);
        axios
          .get(res_url)
          .then(function(response) {
            let data = response.data.response.data;
            // console.log(data.data);
            response_data = pushToArray(response_data, data.data);
            page_count = data.pageCount;
            var reqArray = [];
            if (page_count == 1) {
              getOfferURL(response_data);
            } else {
              while (current_page <= page_count) {
                current_page += 1;
                let req_url = url + "&page=" + current_page;
                reqArray.push(axios.get(req_url));
              }
              axios.all(reqArray).then(function(res) {
                let temp = res.map(function(r) {
                  let data = r.data.response.data;
                  // console.log(data.data);
                  response_data = pushToArray(response_data, data.data);
                  getOfferURL(response_data);
                });
              });
            }
          })
          .catch(function(err) {
            console.log(err);
          });
}


function pushToArray(result, data) {
  data.map(function(dataObj) {
    let d = dataObj.Stat;
    var obj = {};
    obj.Date = d.date;
    obj.creative_url_id = d.creative_url_id || 0;
    if (
      result.offerIds.indexOf(obj.creative_url_id) == -1 &&
      obj.creative_url_id > 0
    ) {
      result.offerIds.push(obj.creative_url_id);
    }
    obj.Impressions = d.impressions || 0;
    obj.Clicks = d.clicks || 0;
    obj.CTR = d.ctr || 0;
    obj.Cost = d.revenue || 0;
    obj.Link = d.url || "APP_AP";
    obj.utm_new_param = d.param || "APP_AP";
    result.data.push(obj);
  });
  return result;
}

function populateURLinResultArray(result, data) {
  result.data.map(function(d) {
    if (d.creative_url_id && d.creative_url_id != 0) {
      let url = data[d.creative_url_id].OfferUrl.offer_url || "";
      d.Link = url;
      d.utm_new_param = url.match(".*utm_content= *(.*?) *&utm_campaign.")[1];
    }
    delete d.creative_url_id;
  });
  return result;
}

function getOfferURL(result_data) {
  // console.log(result_data);
  // console.log(getURLfromOfferIds(result_data.offerIds));
  axios
    .get(getURLfromOfferIds(result_data.offerIds))
    .then(function(res) {
      console.log(res.data.response.data);
      result_data = populateURLinResultArray(
        result_data,
        res.data.response.data
      );
      const fileName = `RummyCircle_Report CPI  for ${moment()
        .subtract(1, "days")
        .format("YYYY-MM-DD")}.csv`;
      jsonToCSV(result_data.data, fileName);
      sendmail(
        {
          from: "reports@conversionx.co",
          to: "vishal.dharmawat@gmail.com,operations@conversionx.co,rajeev@conversionx.co",
          subject: "Trulymadly Daily Report RummyCircle",
          attachments: [
            {
              filename: fileName,
              path: fileName
            }
          ]
        },
        function(err, reply) {
          console.log(err && err.stack);
          console.log(reply);
        }
      );
    })
    .catch(function(err) {
      console.log(err);
    });
}

function makeAPICall(querystring) {
  axios
    .get(querystring)
    .then(function(res) {
      return res;
    })
    .catch(function(err) {
      return err;
    });
}

function getURLfromOfferIds(ids) {
  var BASE_URL =
    "https://conversionx.api.hasoffers.com/Apiv3/json?NetworkToken=NETjrV09cKLuquX09HSJOyJXMuUrGX&Target=OfferUrl&Method=findAllByIds&fields[]=offer_url&sort[OfferUrl.id]=asc";
  ids.map(function(id) {
    BASE_URL = BASE_URL + "&ids[]=" + id;
  });
  return BASE_URL;
}

function getBaseURL(query) {
  var advertiser =
    (query.advertiser).replace(/%20/g, "+") ||
    "Play+Games24x7+Pvt.+Ltd";
  var days = query.days || 1;
  var start_date = moment()
    .subtract(days, "days")
    .format("YYYY-MM-DD");
  var end_date = moment()
    .subtract(1, "days")
    .format("YYYY-MM-DD");
  var BASE_URL = `https://conversionx.api.hasoffers.com/Apiv3/json?NetworkToken=NETjrV09cKLuquX09HSJOyJXMuUrGX&Target=Report&Method=getStats&fields[]=Stat.revenue&fields[]=Stat.creative_url_id&fields[]=Advertiser.company&fields[]=Stat.ctr&fields[]=Stat.impressions&fields[]=Stat.clicks&groups[]=OfferCreativeUrl.name&groups[]=Stat.date&groups[]=Advertiser.company&filters[Advertiser.company][conditional]=EQUAL_TO&filters[Advertiser.company][values]=${
    advertiser
  }&data_start=${start_date}&data_end=${end_date}`;
  return BASE_URL;
}

