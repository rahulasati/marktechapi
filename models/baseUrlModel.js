var db = require('../database/dbconnection');
var jwt = require('jsonwebtoken');
var config = require('../config');

module.exports = {
    getBaseUrls: function (reqData, callback) {
        db.query("SELECT * FROM base_url", function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                callback(null, rows, null);
            }
        });
    }
}