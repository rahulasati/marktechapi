var cron = require('node-cron');
var mail = require('../models/mailReport');

module.exports = {
    playgamesWeekdayReport : cron.schedule('30 5 * * 3-6', function () {
        mail.scheduleReport("Play+Games24x7+Pvt.+Ltd",1);
      }, true),
    playgamesWeekendReport : cron.schedule('30 5 * * 1', function () {
        mail.scheduleReport("Play+Games24x7+Pvt.+Ltd",3);
      }, true)
}


