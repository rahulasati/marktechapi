var db = require('../database/dbconnection');

module.exports = {

    createSegment: function (reqData, callback) {
        db.query("INSERT INTO segment set ?", {
            segment_name: reqData.name,
            segment_desc: reqData.desc || "",
            segment_query: reqData.query || buildQueryFromProps(reqData.props),
            created_by: reqData.user_id
        }, function (err, row) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                console.log(row);
                callback(err, { segment_id: row.insertId }, "Segment Created Successfully.");
            }
        });
    },

    updateSegment: function (reqData, callback) {
        var values = {
            segment_name: reqData.name || undefined,
            segment_desc: reqData.desc || undefined,
            segment_query: reqData.query || buildQueryFromProps(reqData.props),
            created_by: undefined
        };

        values = JSON.stringify(values);
        values = JSON.parse(values);
        console.log(values);

        db.query("UPDATE segment SET ? WHERE id = ?", [values, reqData.segment_id], function (err, row) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                if (row.affectedRows > 0) {
                    callback(null, values, 'Segment Updated Successfully');
                } else {
                    callback('No Matching Segment Found To Update!', null, null);
                }
            }
        });
    },

    getSegments: function (reqData, callback) {
        db.query("SELECT * FROM segment", function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                callback(null, rows, null);
            }
        });
    },

    deleteSegment: function (segment_id, callback) {
        var sql = "DELETE FROM segment WHERE id = " + segment_id;
        db.query(sql, function (err, row) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                if (row.affectedRows > 0) {
                    callback(null, null, 'Segment Deleted Successfully.');
                } else {
                    callback('No Matching Segment Found To Delete!', null, null);
                }
            }
        });
    }
}

function buildQueryFromProps(props) {

}