var db = require("../database/dbconnection");
var cron = require("node-cron");
var axios = require("axios");
var underscore = require("underscore");

module.exports = {
    start: function () {
        cron.schedule(
            process.env.NODE_ENV === 'dev' ? "*/5 * * * * *" : "*/5 * * * * *",
            function () {
                getConversionData();
            },
            true
        )
    }
}

function getConversionData() {
    var date = new Date();
    var previousDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() - 1);
    console.log(previousDate);
    var dateTime = previousDate + "+01%3A00%3A00";
    console.log(dateTime)
    //var dateTime = "2017-12-13+01%3A00%3A00";
    var hasOfferAPI =
        "https://conversionx.api.hasoffers.com/Apiv3/json?NetworkToken=NETjrV09cKLuquX09HSJOyJXMuUrGX&Target=Conversion&Method=findAll&filters[datetime][GREATER_THAN]=" +
        dateTime;
    axios
        .get(hasOfferAPI)
        .then(function (res) {
            console.log("Received Response From Has Offer");
            //console.log(res.data);
            var arr = res.data.response.data;
            for (var key in arr) {
                if (arr.hasOwnProperty(key)) {
                    var conversion_id = arr[key].Conversion.id;
                    var affiliate_info1 = arr[key].Conversion.affiliate_info1;
                    console.log("conversionId: "+conversion_id+" & affiliate_info1 : "+affiliate_info1);
                }
            }
            //updateConversion();
        })
        .catch(function (err) {
            console.log("Error in Getting Response from has offers");
            console.log(err);
        });
}

function updateConversion(data) {

    values = {
        conversion_timestamp: data.conversion_timestamp
    };

    db.query("Update campaign_report SET ? where unique_path = ?", [values, data.path], function (err, row) {
        if (err) {
            console.log(err);
        } else {
            console.log(row);
        }
    }
    );
}