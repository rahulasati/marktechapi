var db = require("../database/dbconnection");
var cron = require("node-cron");
var axios = require("axios");
var underscore = require("underscore");

module.exports = {
  start: function () {
    cron.schedule(
      process.env.NODE_ENV === 'dev' ? "*/1 * * * * *" : "*/1 * * * *",
      function () {
        //checkQueue();
      },
      true
    )
  }
}

function checkQueue() {
  var query =
    "select * from queue where schedule_time between unix_timestamp()-600 and unix_timestamp()+600 and status = 'scheduled' LIMIT 100";
  db.query(query, function (err, rows) {
    if (err) {
      console.log(err);
    } else {
      if (rows.length > 0) {
        markPicked(underscore.pluck(rows, "id"));
        rows.map(function (row, index) {
          console.log('Row data -');
          console.log(row);
          if (row.target_type == "phone") {
            generateUniqueURL(row, function (res, err) {
              if (err) {
                console.log("Error in Generate URL in checkQueue");
                console.log(err);
              } else {
                console.log("Creating base url-----");
                row.base_url = res.base_url;
                row.url_path = res.url_path;
                console.log("Base URL is - " + row.base_url);
                sendSMS(row);
              }
            });
          }
        });
      } else {
        console.log("------Media Queue is empty----------");
      }
    }
  });
}

function markPicked(idsArray) {
  console.log('Marking status as picked - ' + idsArray);
  var query =
    "UPDATE queue SET status = 'picked' where id in(" + idsArray.join() + ")";
  db.query(query, function (err, rows) {
    if (err) {
      console.log(err);
    } else {
      console.log("Updated Status to picked");
    }
  });
}

function sendSMS(row) {
  var phone = row.target_value.substring(row.target_value.length - 10);
  let data = {};
  data.user_id = row.user_id;
  data.campaign_id = row.campaign_id;
  addToCampaignSent(row, function (err, res) {
    if (err) {
      console.log("Error in adding campaign in sendSMS");
      console.log(err);
    } else {
      console.log("Added new Campaign for the user");
      console.log(row);
      let campaignreport_id = res.insertId;
      let unique_path = row.url_path + campaignreport_id.toString(36);
      var fullPath = row.base_url + unique_path;
      var message = row.media_value;
      if (fullPath) {
        message += " " + fullPath;
      }
      console.log("MESSAGE TO SEND IN SMS - " + message);
      var smsLeadAPI =
        "http://smsleads.in/pushsms.php?username=utsav1234&password=maw@1755&sender=" +
        row.sms_sender_id +
        "&numbers=" +
        phone +
        "&message=" +
        message;
      axios
        .get(smsLeadAPI)
        .then(function (res) {
          //On successfully scheduling message with SMS vendor, mark that campaign as sent.
          console.log("SMS Sent Successfully...updating sent time");
          updateCampaignSent(campaignreport_id, unique_path);
        })
        .catch(function (err) {
          console.log("Error in sending SMS");
          console.log(err);
        });
    }
  });
}

function generateUniqueURL(row, callback) {
  console.log('Generating a Unique URL');
  db.query(
    "SELECT * from campaign_schedule where id = " + row.campaign_id,
    function (err, row) {
      if (err) {
        console.log('Generating a Unique URL:Error');
        callback(null, err);
      } else {
        console.log('Generating a Unique URL:Success');
        callback(row[0], null);
      }
    }
  );
}

function addToCampaignSent(row, callback) {
  let data = {};
  data.user_id = row.user_id;
  data.campaign_id = row.campaign_id;
  db.query("INSERT INTO campaign_report set ?", data, function (err, row) {
    if (err) {
      console.log("Error in adding a campaign");
      console.log(err);
      callback(err, null);
    } else {
      console.log("Adding to Campaign sent successful.");
      console.log(row);
      callback(null, row);
    }
  });
}

function updateCampaignSent(rowid, unique_path) {
  let current_timestamp = Math.round(new Date().getTime() / 1000);
  console.log(rowid);
  values = {
    sent_timestamp: current_timestamp,
    unique_path: unique_path
  }
  db.query(
    'Update campaign_report SET ? where id = ?', [values, rowid],
    function (err, row) {
      if (err) {
        console.log(err);
      } else {
        console.log(row);
      }
    }
  );
}
