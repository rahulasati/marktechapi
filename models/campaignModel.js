var db = require("../database/dbconnection");
var values = require('object.values');

module.exports = {
  sendBySegment: function (reqData, callback) {
    return db.query(
      "INSERT INTO sms_content set ?",
      {
        content: reqData.message,
        sender: reqData.sender_id
      },
      function (err, row) {
        if (err) {
          console.log(err);
          callback(err, null);
        } else {
          var values = {
            segment_type: "segment",
            segment_id: reqData.segment_id,
            content_type: "sms",
            content_id: row.insertId,
            created_by: reqData.user_id,
            schedule_time: reqData.time || -1,
            campaign_name: reqData.campaign_name,
            url_path: reqData.url_path,
            base_url: reqData.base_url,
            redirect_url: reqData.redirect_url,
            tags: reqData.tags || ""
          };
          return db.query("INSERT INTO campaign_schedule set ?", values, function (
            err,
            row
          ) {
            if (err) {
              console.log(err);
              callback(err, null);
            } else {
              callback(err, row);
              insertIntoQueue(row.insertId);
            }
          });
        }
      }
    );
  },

  sendByNumbers: function (reqData, callback) {
    db.query(
      "INSERT INTO segment_adhoc set ?",
      {
        target_type: "phone",
        target_value: reqData.numbers,
        created_by: reqData.user_id
      },
      function (err, row) {
        if (err) {
          console.log(err);
          callback(err, null);
        } else {
          var segmentId = row.insertId;
          db.query(
            "INSERT INTO sms_content set ?",
            {
              content: reqData.message,
              sender: reqData.sender_id
            },
            function (err, row) {
              if (err) {
                console.log(err);
                callback(err, null);
              } else {
                var values = {
                  segment_type: "segment_adhoc",
                  segment_id: segmentId,
                  content_type: "sms",
                  content_id: row.insertId,
                  created_by: reqData.user_id,
                  schedule_time: reqData.time || -1
                };
                db.query("INSERT INTO schedule_media set ?", values, function (
                  err,
                  row
                ) {
                  if (err) {
                    console.log(err);
                    callback(err, null);
                  } else {
                    callback(err, row);
                    insertIntoQueue(row.insertId);
                  }
                });
              }
            }
          );
        }
      }
    );
  }
};

function insertIntoQueueValues(value) {
  //For phone numbers only -

  var userArray = value.users;
  var insertValueArray = [];
  userArray.map(function (user, index) {
    var temp = Object.assign({}, value);
    delete temp.users;
    temp.target_value = user.phone;
    temp.user_id = user.id;
    //var tempArray = Object.values(temp);
    var tempArray = values(temp);
    insertValueArray.push(tempArray);
  });
  console.log('--------insertIntoQueueValues---------------');
  console.log(insertValueArray);
  db.query(
    "INSERT into queue (campaign_id,schedule_time,status,target_type,media_type,media_value,sms_sender_id,target_value,user_id) VALUES ?",
    [insertValueArray],
    function (err, row) {
      if (err) {
        console.log(err);
      } else {
        console.log(row);
      }
    }
  );
}

function insertIntoQueue(campaign_id) {
  db.query("SELECT * from campaign_schedule where id = " + campaign_id, function (err, rows) {
    if (err) {
      console.log(err);
      callback(err, null);
    } else {
      var media = rows[0];
      console.log('---------insertIntoQueue-------');
      console.log(media);
      var insertQueueValues = {};
      insertQueueValues.campaign_id = campaign_id;
      insertQueueValues.schedule_time = media.schedule_time;
      insertQueueValues.status = "scheduled";
      var contentTableName = "";

      switch (media.content_type) {
        case "sms": {
          insertQueueValues.target_type = "phone";
          contentTableName = "sms_content";
          insertQueueValues.media_type = "sms";
          break;
        }
        default: {
          insertQueueValues.target_type = "phone";
          contentTableName = "sms_content";
          insertQueueValues.media_type = "sms";
          break;
        }
      }
      db.query(
        `select * from ${contentTableName} where id=${media.content_id}`,
        function (err, row) {
          if (err) {
            console.log(err);
          } else {
            var mediaContent = row[0];
            console.log('---------contentTable-------');
            console.log(mediaContent);
            if (insertQueueValues.media_type === "sms") {
              insertQueueValues.media_value = mediaContent.content;
              insertQueueValues.sms_sender_id = mediaContent.sender;
            } else {
              //TODO: Email logic here...
            }
            switch (media.segment_type) {
              case "segment":
                db.query("SELECT segment_query from segment where id = " + media.segment_id, function (err, rows) {
                  if (err) {
                    console.log(err);
                  } else {
                    var segmentQuery = rows[0].segment_query;
                    console.log('---------segmentQuery-------');
                    console.log(segmentQuery);
                    db.query(segmentQuery, function (err, rows) {
                      if (err) {
                        console.log(err);
                      } else {
                        console.log('---------got user phone numbers-------');
                        insertQueueValues.users = rows;
                        insertIntoQueueValues(insertQueueValues);
                      }
                    });
                  }
                }
                );
                break;
              case "segment_adhoc":
                db.query(
                  "SELECT target_value from segment_adhoc where id = " +
                  media.segment_id,
                  function (err, rows) {
                    if (err) {
                      console.log(err);
                    } else {
                      var phones = rows[0].target_value;
                      insertQueueValues.phone_numbers = phones.split(",");
                      insertIntoQueueValues(insertQueueValues);
                    }
                  }
                );
                break;
              default:
                break;
            }
          }
        }
      );
    }
  });
}