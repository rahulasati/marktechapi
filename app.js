var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var auth = require('./routes/auth');
var track = require('./routes/track');
var campaign = require('./routes/campaign');
var segment = require('./routes/segment');
var home = require('./routes/home');
var mailreport = require('./routes/mailreport');
var reportCron = require('./models/reportCron');
var csv2sql = require('./routes/csv2sql');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', home);
app.use('/api', home);
app.use('/api/auth', auth);
app.use('/api/track', track);
app.use('/api/campaign', campaign);
app.use('/api/segment', segment);
app.use('/api/mailreport', mailreport);
app.use('/api/data/dump', csv2sql);

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  // var err = new Error('Not Found');
  // err.status = 404;
  // next(err);
  res.locals.message = "Page Not Found";
  res.locals.error = {};
  res.status(404);
  res.render('error');
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//reportCron.playgamesWeekdayReport;

module.exports = app;
