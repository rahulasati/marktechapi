var mysql = require('mysql');
//var configValues = require('./dbconfig');
//var configValues = require('./dbconfig_local');
var configValues = require('./dbconfig_dev');
//var configValues = require('./dbconfig_temp');

var connection = mysql.createPool({
    connectionLimit : 1000,
    connectTimeout  : 60 * 60 * 1000,
    aquireTimeout   : 60 * 60 * 1000,
    timeout         : 60 * 60 * 1000,
    host: configValues.host,
    user: configValues.user,
    password: configValues.password,
    database: configValues.database
});

module.exports = connection;